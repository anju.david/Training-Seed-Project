import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { TokenInterceptor } from "./common/interceptor";


// Plugins
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { BsDatepickerModule, ModalModule,TabsModule} from 'ngx-bootstrap';
import { BnDatatableModule } from './common/bn-datatable/bn-datatable.module'
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgSelectModule } from '@ng-select/ng-select';
import { OwlModule } from 'ngx-owl-carousel';
import { MalihuScrollbarModule } from "ngx-malihu-scrollbar";

// Common
import { BaseComponent } from './common/commonComponent';
import { CommonService } from './common/common.service';
import { CanLoginActivate, CanAuthActivate } from './common/auth.gaurd';
import { ErrorMessages } from './common/errorMessages';
import { BsModalService } from 'ngx-bootstrap/modal';

// Public pages
import { HomeComponent } from './public/home/home.component';
import { HeaderComponent } from './public/header/header.component';
import { FooterComponent } from './public/footer/footer.component';
// import { AboutComponent } from './public/about/about.component';

// Auth pages
import { AuthHeaderComponent } from './reusable/auth-header/auth-header.component';
import { TeacherDashboardComponent } from './teacher/teacher-dashboard/teacher-dashboard.component';
import { TeacherInfoComponent } from './teacher/teacher-info/teacher-info.component';
import { TeacherDetailComponent } from './teacher/teacher-detail/teacher-detail.component';
import { TeacherDetail1Component } from './teacher/teacher-detail1/teacher-detail1.component';

const routes=[
{
  path: "",
  component: HomeComponent,
  pathMatch: "full"
},
{
  path: "teacher",
  component: TeacherDashboardComponent,   
  children:[
  {
    path: "teacherInfo",
    component: TeacherInfoComponent,
    pathMatch: "full"
  },{
    path: "teacherDetail/:id",
    component: TeacherDetailComponent,
    pathMatch: "full"
  },{
    path: "teacherDetail-1",
    component: TeacherDetail1Component,
    pathMatch: "full"
  }
  ]
}, 
{ path: "**", redirectTo: "/", pathMatch: "full" }
]

@NgModule({
  declarations: [
  AppComponent,
  HomeComponent,
  BaseComponent,
  CanLoginActivate,
  CanAuthActivate,
  HeaderComponent,
  FooterComponent,
  AuthHeaderComponent,
  TeacherDashboardComponent,
  TeacherInfoComponent,
  TeacherDetailComponent,
  TeacherDetail1Component
  ],
  imports: [
  MalihuScrollbarModule.forRoot(),
  NgxSpinnerModule,
  BnDatatableModule,
  ModalModule.forRoot(),
  NgSelectModule,
  TabsModule.forRoot(),
  OwlModule,
  BsDatepickerModule.forRoot(),
  LoadingBarHttpClientModule,
  FormsModule,
  BrowserModule.withServerTransition({ appId: "my-app" }),
  HttpClientModule,
  BrowserTransferStateModule,
  RouterModule.forRoot(routes)
  ],
  providers: [
  CanLoginActivate,
  CanAuthActivate,
  CommonService,
  ErrorMessages,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
