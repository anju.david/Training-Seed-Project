import { Component, PLATFORM_ID, Injectable, NgZone, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { BaseComponent } from './../common/commonComponent';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import {config} from '../../assets/config/configs';
import swal from 'sweetalert2';

@Injectable({
  providedIn: "root"
})
export class CommonService {
  authorised: any = false;
  constructor(
    public _http: HttpClient,
    @Inject(PLATFORM_ID) platformId: Object
  ) {
    this.platformId = platformId;
    this._apiUrl = this.config.apiUrl;

    //  this._http
    //    .get("https://jsonplaceholder.typicode.com/posts")
    //    .subscribe(data => console.log(data), err => console.log(err));
  }

  public swal = swal;
  public config = <any>config;
  public _apiUrl = "";
  public platformId;

  public getToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      return window.localStorage.getItem(key);
    }
  }
  public setToken(key, value) {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.setItem(key, value);
    }
  }

  /*******************************************************************************************
      @PURPOSE      	: 	Call api.
      @Parameters 	: 	{
            url : <url of api>
            data : <data object (JSON)>
            method : String (get, post)
            isForm (Optional) : Boolean - to call api with form data
            isPublic (Optional) : Boolean - to call api without auth header
          }
  /*****************************************************************************************/
  callApi(url, data, method, pagination?, isPublic?): Promise<any> {
    return new Promise((resolve, reject) => {
      if (method === "post") {
        this._http.post(this._apiUrl + url, data).subscribe(
          data1 => {
            resolve(data1);
          },
          error => {
            this.showServerError(error);
          }
        );
      } else if (method === 'get') {
        // let params: { appid: 'id1234', cnt: '5' }

        if (data) {
          let params = new HttpParams();
          Object.keys(data).forEach(function(key) {
            params = params.set(key, data[key]);
          });
          this._http
            .get(this._apiUrl + url, { params })
            .subscribe(
              data1 => {
                resolve(data1);
              },
              error => {
                this.showServerError(error);
              }
            );
        } else {
          this._http
            .get(this._apiUrl + url)
            .subscribe(
              data1 => {
                resolve(data1);
              },
              error => {
                this.showServerError(error);
              }
            );
        }


      }
    });
  }

  /*****************************************************************************************/
  // @PURPOSE      	: 	To show server error
  /*****************************************************************************************/
  showServerError(e) {
    this.swal({
      position: 'center',
      type: 'error',
      text: 'Internal Server Error',
      showConfirmButton: false,
      timer: 1800,
      customClass: 'custom-toaster'
    });
    console.log('Internal server error', e);
  }
  /****************************************************************************/
}


