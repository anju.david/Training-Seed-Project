import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";
import { CommonService } from "./common.service";
import { Observable } from "rxjs/Observable";
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public commonService: CommonService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    //console.log("Interceptor called", request)

    if (this.commonService.getToken("accessToken")) {
      request = request.clone({
        setHeaders: {
          Authorization: `${this.commonService.getToken(
            "accessToken"
          )}`
        }
      });
    } else {
      request = request.clone();
    }

    return next.handle(request);
  }
}
