import { Component, OnInit, TemplateRef, Injector } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsDatepickerConfig} from 'ngx-bootstrap/datepicker';
import { BaseComponent } from './../../common/commonComponent'

declare var jQuery: any;
declare var $: any;

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent extends BaseComponent implements OnInit {
  // Datepicker
  colorTheme = "theme-red";
  bsConfig: Partial<BsDatepickerConfig>;
  isOpen = false;

  // Modal
  modalRef: BsModalRef;
  modalRefForgot: BsModalRef;
  public Type = ["Midtearm", "Type 2", "Type 3"];
  public Term = ["1", "2", "3"];
  public Year = ["2018", "2017", "2016"];
  constructor(inj: Injector) {
    super(inj);
    this.bsConfig = Object.assign(
      {},
      {
        containerClass: this.colorTheme
      }
      );
  }

  onActivate() {}
  ngOnInit() {
    // this.spinner.show()
  }

  // Nav Transform
  toggle() {
    $(".nav-toggle").toggleClass("active");
    $("header nav").toggleClass("open");
  }

  // Sticky Header on Scroll
  myScrollHandler() {
    const scroll = $(window).scrollTop();
    if (scroll >= 100) {
      $("header").addClass("fixed");
    } else {
      $("header").removeClass("fixed");
    }
  }

  removeOpen() {
    $("header nav").removeClass("open");
    $(".nav-toggle").removeClass("active");
  }

  openLoginModal(template: TemplateRef<any>) {
    if (this.getToken("ss_id") != null && this.getToken("ss_pass") != null) {
      this.user = {
        username: this.getToken("ss_id"),
        password: this.getToken("ss_pass"),
        remember: true
      };
    } else {
      this.submitted1 = false;
      this.user = {}
    }
    this.modalRef = this.modalService.show(template, {
      class: "modal-dialog-centered auth-modal"
    });
  }

  /*************************************************************/
  // Forgot Password Start
  /*************************************************************/
  openForgotModal(template: TemplateRef<any>) {
    this.modalRef.hide();
    this.modalRefForgot = this.modalService.show(template, {
      class: "modal-dialog-centered auth-modal"
    });
    this.user = {}
    this.submitted2 = false;
  }

  onForgotPass(form, data) {
    this.submitted2 = true;
    if (form.valid) {
      this.spinner.show();
      const user = { userId: data.username };
      this.commonService
      .callApi("user/forgotPassword", user, "post", "", true)
      .then(success => {
        this.spinner.hide();
        if (success.status === 1) {
          this.modalRefForgot.hide();
          this.popToast("success", success.message);
        } else {
          this.popToast("error", success.message);
          this.submitted2 = false;
        }
      });
    }
  }
  /*************************************************************/

  /*************************************************************/
  // User Login
  /*************************************************************/
  user: any = {};
  submitted1: Boolean;
  submitted2: Boolean;
  onLogin(form, data) {
    this.submitted1 = true;
    if (form.valid) {
      // this.spinner.show();
      const user = {
        userid: data.username,
        password: data.password,
        userProperties: ["primaryImageUrl", "Email", "UserEntityCorrelationId"]
      };
      this.modalRef.hide();
      // if (data.username === "schooladmin") {
      //   this.setToken("accessToken", "testadmin");
      //   this.setToken("ss_cls_id", "CLSS_1002");
      //   this.setToken("ss_entity_id", "TCHR_1061");
      //   this.setToken("ss_name", "School Admin");
      //   this.setToken("ss_role", "teacher");
      //   this.router.navigate(["/admin"]);
      //   console.log("school admin");
      //   this.spinner.hide();
      //   this.modalRef.hide();

      //   return 0;
      // }

      // if (data.remember) {
      //   this.setToken("ss_id", data.username);
      //   this.setToken("ss_pass", data.password);
      // } else {
      //   this.removeToken("ss_id");
      //   this.removeToken("ss_pass");
      // }
      // this.commonService
      //   .callApi("user/login", user, "post", "", true)
      //   .then(success => {
      //     this.spinner.hide();
      //     if (success.status === 1) {
      //       this.modalRef.hide();
      //       localStorage.setItem("accessToken", success.token);
      //       localStorage.setItem("ss_name", success.name);
      //       if (success.userProperties.primaryImageUrl) {
      //         localStorage.setItem(
      //           "ss_pic",
      //           success.userProperties.primaryImageUrl
      //         );
      //       }
      //       if (success.roles[0] === "Teacher") {
      //         this.setToken(
      //           "ss_entity_id",
      //           success.userProperties.UserEntityCorrelationId
      //         );
      //         this.setToken("ss_role", "teacher");
      //         if (success.userProperties) {
      //           if (success.userProperties.ClassId) {
      //             const ss_cls_id = success.userProperties.ClassId;
      //             const ss_cls_name = success.userProperties.ClassName;
      //             this.setToken("ss_cls_id", ss_cls_id);
      //             this.setToken("ss_cls_name", ss_cls_name);
      //             this.router.navigate(["/teacher"], {
      //               queryParams: { class: ss_cls_id }
      //             });
      //             return 0;
      //           }
      //         }
      this.router.navigate(["/teacher"]);
          //   } else if (success.roles[0] === "SystemAdminGroup") {
          //            this.setToken("ss_entity_id", success.userProperties.UserEntityCorrelationId);
          //            this.setToken("ss_role", "schooladmin");
          //            this.router.navigate(["/admin"]);
          //            // if (success.userProperties) {
          //            //   if (success.userProperties.ClassId) {
          //            //     const ss_cls_id = success.userProperties.ClassId;
          //            //     const ss_cls_name = success.userProperties.ClassName;
          //            //     this.setToken("ss_cls_id", ss_cls_id);
          //            //     this.setToken("ss_cls_name", ss_cls_name);
          //            //   }
          //            // }
          //          } else if (success.roles[0] === "Parent") {
          //            this.setToken("ss_entity_id", "TCHR_1061");
          //            this.setToken("ss_role", "parent");
          //            console.log("navigating to guardian")
          //            this.router.navigate(["/guardian"]);
          //            // if (success.userProperties) {
          //            //   if (success.userProperties.ClassId) {
          //            //     const ss_cls_id = success.userProperties.ClassId;
          //            //     const ss_cls_name = success.userProperties.ClassName;
          //            //     this.setToken("ss_cls_id", ss_cls_id);
          //            //     this.setToken("ss_cls_name", ss_cls_name);
          //            //   }
          //            // }
          //          } else {
          //            this.router.navigate(["/teacher"]);
          //          }
          // } else {
          //   this.popToast("error", success.message);
          //   this.submitted1 = false;
          // }
        // });
      }
    }
    /*************************************************************/
  }

