import { Component, OnInit ,Injector,TemplateRef} from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']

})
export class HomeComponent extends BaseComponent  implements OnInit {

  constructor(inj:Injector) {
    super(inj);
  }

  ngOnInit() { 
  }
  
  ngAfterViewInit() {
  }

}
