import { Component, OnInit , TemplateRef, Injector, Input, AfterViewInit } from '@angular/core';
// import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BaseComponent } from './../../common/commonComponent'
declare var jQuery: any;
declare var $: any;
@Component({
  selector: "app-auth-header",
  templateUrl: "./auth-header.component.html",
  styleUrls: ["./auth-header.component.css"]
})
export class AuthHeaderComponent extends BaseComponent implements OnInit {
  @Input("role") role: any;
  modalRefChangePassModal: BsModalRef;
  constructor(inj: Injector) {
    super(inj);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    console.log("current role : ", this.role)
  }

  ngOnInit() {}

  /*************************************************************/
  // Open Change Password Model
  /*************************************************************/
  openChangePassModal(template: TemplateRef<any>) {
    this.modalRefChangePassModal = this.modalService.show(template, {
      class: "modal-dialog-centered modal-md"
    });
    this.user = {};
    this.submitted = false;
  }
  /*************************************************************/

  /*************************************************************/
  // // Submit change password
  /*************************************************************/
  user: any = {};
  submitted: Boolean;
  onChangePassword(form, data) {
    this.submitted = true;
    if (form.valid) {
      this.spinner.show();
      const user = {
        currentPassword: data.currentPassword,
        newPassword: data.newPassword
      };
      // this.commonService
      //   .callApi("user/resetPassword", user, "post")
      //   .then(success => {
      //     this.spinner.hide();
      //     if (success.status === 1) {
      //       this.modalRefChangePassModal.hide();
      //       this.popToast("success", success.message);
      //     } else {
      //       this.popToast("error", "Current password is incorrect.");
      //       this.submitted = false;
      //     }
      //   });
    }
  }
  /*************************************************************/
}
