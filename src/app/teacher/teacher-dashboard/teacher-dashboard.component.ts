import { Component, OnInit, Injector,ViewChild } from '@angular/core';
import { BaseComponent } from '../../common/commonComponent'
import { TabsetComponent } from 'ngx-bootstrap';
declare var jQuery: any;
declare var $: any;

@Component({
	selector: "app-teacher-dashboard",
	templateUrl: "./teacher-dashboard.component.html",
	styleUrls: ["./teacher-dashboard.component.css"]
})
export class TeacherDashboardComponent extends BaseComponent implements OnInit {
	@ViewChild('staticTabs') staticTabs: TabsetComponent;
	constructor(inj: Injector) {
		super(inj);
		
	}

	ngOnInit() {
	}

	selectTab(tab_id: number) {
		this.staticTabs.tabs[tab_id].active = true;
	}
	onClick(){
		this.router.navigate(['/teacher/teacherInfo']);
	}
}
