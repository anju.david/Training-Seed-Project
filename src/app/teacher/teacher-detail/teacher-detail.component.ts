import { Component, OnInit,Injector,ViewChild  } from '@angular/core';
import { BaseComponent } from '../../common/commonComponent'
declare var jQuery: any;
declare var $: any;
@Component({
	selector: 'app-teacher-detail',
	templateUrl: './teacher-detail.component.html',
	styleUrls: ['./teacher-detail.component.css']
})
export class TeacherDetailComponent extends BaseComponent implements OnInit {
	public getParam:any;
	constructor(inj: Injector) {
		super(inj);
		// this.getParam=this.activatedRoute.snapshot.paramMap.get('id');
		// console.log("this.getParam",this.getParam);
		this.activatedRoute.params.subscribe((params)=>{
			console.log("value==",params);
			this.getParam=params;
		})

	}

	ngOnInit() {
	}
	ngAfterViewInit(){
		// this.activatedRoute.queryParams.subscribe((params)=>{
		// 	console.log("value==",params);
		// 	this.getParam=params;
		// })
	}

}
