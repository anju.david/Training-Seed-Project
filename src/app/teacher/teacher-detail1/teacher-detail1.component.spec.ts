import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherDetail1Component } from './teacher-detail1.component';

describe('TeacherDetail1Component', () => {
  let component: TeacherDetail1Component;
  let fixture: ComponentFixture<TeacherDetail1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherDetail1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherDetail1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
