import { Component, OnInit ,Injector,ViewChild } from '@angular/core';
import { BaseComponent } from '../../common/commonComponent'
declare var jQuery: any;
declare var $: any;
@Component({
	selector: 'app-teacher-detail1',
	templateUrl: './teacher-detail1.component.html',
	styleUrls: ['./teacher-detail1.component.css']
})
export class TeacherDetail1Component extends BaseComponent implements OnInit {
	public getParam:any;
	constructor(inj: Injector) {
		super(inj);
		// this.getParam=this.activatedRoute.snapshot.paramMap.get('id');
		// console.log("this.getParam",this.getParam);
		this.activatedRoute.queryParams.subscribe((params)=>{
			console.log("value==",params);
			this.getParam=params;
		})

	}
	ngOnInit() {
	}

}
